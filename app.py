from flask import Flask,render_template,redirect,url_for,session,request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
app.config['SQL_TRACK_MODIFICATIONS'] = False

app.config['SQLALCHEMY_BINDS'] = {
    'blogg':'sqlite:///blogg.db'
}

db = SQLAlchemy(app)


class user(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    email = db.Column(db.String(120))
    password = db.Column(db.String(80))


class blogg(db.Model):
    __bind_key__='blogg'
    id = db.Column(db.Integer, primary_key=True)
    authors = db.Column(db.String(20))
    blogs = db.Column(db.String(2000))

@app.route("/")
def index():
    return render_template("index.html")
app.config['SQL_TRACK_MODIFICATIONS'] = False

@app.route("/login",methods=["GET", "POST"])
def login():
    if request.method == "POST":
        uname = request.form["uname"]
        passw = request.form["passw"]
        
        login = user.query.filter_by(username=uname, password=passw).first()
        if login is not None:
            return redirect(url_for("blog"))
    return render_template("login.html")

@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        uname = request.form['uname']
        mail = request.form['mail']
        passw = request.form['passw']

        register = user(username = uname, email = mail, password = passw)
        db.session.add(register)
        db.session.commit()

        return redirect(url_for("login"))
    return render_template("register.html")

@app.route("/blog", methods = ["GET", "POST"])
def blog():


    if request.method == "POST":
        author = request.form["author"]
        blogged = request.form["blogged"]
        blog = blogg(authors = author, blogs = blogged)
        db.session.add(blog)
        db.session.commit()
        
    all = blogg.query.all()
    return render_template("blog.html",all=all)

@app.route("/delete/<int:id>")
def delete(id):
    blog = blogg.query.filter_by(id=id).first()
    db.session.delete(blog)
    db.session.commit()
    return redirect("/blog")


if __name__ == "__main__":
    app.run(debug=True)  